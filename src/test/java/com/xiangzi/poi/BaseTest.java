package com.xiangzi.poi;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseTest {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Before
	public void setUp() throws Exception {
		logger.debug("setUp...");
	}

	@After
	public void tearDown() throws Exception {
		logger.debug("tearDown...");
	}

	@Ignore
	@Test
	public void method() {
		logger.debug("Test method...");
	}
	
}
