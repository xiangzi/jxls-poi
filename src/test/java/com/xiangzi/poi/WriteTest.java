package com.xiangzi.poi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.junit.Ignore;
import org.junit.Test;

public class WriteTest extends BaseTest {

	@Ignore
	@Test
	public void write() {
		logger.debug("write start...");

		try {
			OutputStream out = new FileOutputStream("./files/write.xls");

			HSSFWorkbook wb = new HSSFWorkbook();// 创建Excel工作簿对象
			HSSFSheet sheet = wb.createSheet("Sheet1");// 创建Excel工作表对象

			HSSFRow row = sheet.createRow((short) 0); // 创建Excel工作表的行
			CellStyle cellStyle = wb.createCellStyle();// 创建单元格样式
			row.createCell((short) 0).setCellStyle(cellStyle); // 创建Excel工作表指定行的单元格
			row.createCell((short) 0).setCellValue(1); // 设置Excel工作表的值

			row.createCell(2, CellType.NUMERIC).setCellValue(2.5);
			row.createCell(3).setCellValue(Double.parseDouble("100.00"));
			row.createCell(4, CellType.NUMERIC).setCellValue(Double.parseDouble("100.00"));

			HSSFDataFormat hssfDataFormat = wb.createDataFormat();
			HSSFCellStyle accountStyle = wb.createCellStyle();// 创建个workbook的HSSFCellStyle格式对象style
			// accountStyle.setDataFormat(hssfDataFormat.getFormat("_ *
			// #,##0.00_ ;_ * -#,##0.00_ ;_ * -??_ ;_ @_ "));
			accountStyle.setDataFormat(hssfDataFormat.getFormat("0.00_ "));// 数字
																			// -
																			// 数值格式

			// row.createCell(5,
			// accountStyle.getDataFormat()).setCellValue(Double.parseDouble("200.00"));

			HSSFCell cell = row.createCell(6);
			cell.setCellValue(Double.parseDouble("200.00"));
			cell.setCellStyle(accountStyle);

			wb.write(out);
			wb.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		logger.debug("write end...");
	}

	@Ignore
	@Test
	public void read() {
		logger.debug("read start...");

		try {
			POIFSFileSystem poifsFileSystem = new POIFSFileSystem(new FileInputStream("./files/read.xls"));
			HSSFWorkbook workbook = new HSSFWorkbook(poifsFileSystem);
			HSSFSheet sheet = workbook.getSheetAt(0);
			for (int k = 0; k <= sheet.getLastRowNum(); k++) {
				HSSFRow row = sheet.getRow(k);
				short minColIx = row.getFirstCellNum();
				short maxColIx = row.getLastCellNum();

				logger.error("minColIx:" + minColIx);
				logger.error("maxColIx:" + maxColIx);

				for (short colIx = minColIx; colIx < maxColIx; colIx++) {
					HSSFCell cell = row.getCell(colIx);

					logger.error("cell:" + cell);
					// logger.error("cell.CellType:" + cell.getCellType());
					logger.error("cell.CellStyle:" + cell.getCellStyle());
					logger.error("cell.CellStyle:" + cell.getCellStyle().getDataFormatString(workbook));

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.debug("read end...");
	}

}
